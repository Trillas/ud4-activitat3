
public class Activitat3 {

	public static void main(String[] args) {
		//Declaro las variables
		int x = 5, y = 7;
		double n = 6.2, m = 2.8;
		//Muestro todas las operaciones
		System.out.println("valorX:" + x + " ValorY:" + y);
		System.out.println("ValorN:" + n + " ValorM:" + m);
		System.out.println(x + " + " + y + " = " + (x + y));
		System.out.println(x + " - " + y + " = " + (x - y));
		System.out.println(x + " * " + y + " = " + (x * y));
		System.out.println(x + " / " + y + " = " + (x / y));
		System.out.println(x + " % " + y + " = " + (x % y));
		System.out.println(n + " + " + m + " = " + (n + m));
		System.out.println(n + " - " + m + " = " + (n - m));
		System.out.println(n + " * " + m + " = " + (n * m));
		System.out.println(n + " / " + m + " = " + (n / m));
		System.out.println(n + " % " + m + " = " + (n % m));
		System.out.println(x + " + " + n + " = " + (x + n));
		System.out.println(y + " / " + m + " = " + (y / m));
		System.out.println(y + " % " + m + " = " + (y % m));
		System.out.println("Doble de " + x + ": " + x * 2);
		System.out.println("Doble de " + y + ": " + y * 2);
		System.out.println("Doble de " + n + ": " + n * 2);
		System.out.println("Doble de " + m + ": " + m * 2);
		System.out.println("Suma de todas las variables: " + (x + y + n + m));
		System.out.println("El producto de todas las variables: " + (x * y * n * m));

	}

}
